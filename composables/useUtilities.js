export const useUtilities = () => {
    function capitalizeFirstLetters (str) {
        return str.split(' ').map(word => word.charAt(0).toUpperCase() + word.slice(1)).join(' ')
    }

    return {
        capitalizeFirstLetters,
    }
}

